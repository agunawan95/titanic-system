import configparser
import mariadb
import csv
import sys

config = configparser.ConfigParser()
config.read('migrate.conf')

data_file_path = '../data/train.csv'

# Connect to MariaDB Platform
try:
    conn = mariadb.connect(
        user= config['database']['username'],
        password=config['database']['password'],
        host=config['database']['host'],
        port=int(config['database']['port']),
        database=config['database']['database']

    )
except mariadb.Error as e:
    print(f"Error connecting to MariaDB Platform: {e}")
    sys.exit(1)

# Get Cursor
cur = conn.cursor()

with open(data_file_path, 'r') as f:
    header = f.readline().replace('\n', '').replace('\r', '')
    header_data = header.split(',')

    for line in csv.reader(f, quotechar='"', delimiter=',', quoting=csv.QUOTE_ALL, skipinitialspace=True):
        d = dict(zip(header_data, line))
        print(d)

        # Insert Procedure
        sql = "INSERT INTO passanger VALUES(default, ?, ?, ?, ?, default)"
        
        try:
            cur.execute(sql, (d['Name'], d['Pclass'], d['Sex'], d['Age']))
        except mariadb.Error as e: 
            print(f"Error: {e}")
        
        conn.commit()

conn.close()

