from flask import Flask, jsonify

import urllib.request
import json

app = Flask(__name__)

@app.route('/')
def home():
    return 'Hello World!'

@app.route('/api/predict/<id>')
def predict(id):
    passanger_url = f"http://passanger:5000/api/passanger/{id}"
    with urllib.request.urlopen(passanger_url) as response:
        result = response.read()
        data = json.loads(result)

    predictor_url = f"http://predictor:5000/api/titanic"
    req =  urllib.request.Request(predictor_url, data=str(json.dumps(data)).encode('utf-8'))
    req.add_header('Content-Type', 'application/json')
    with urllib.request.urlopen(req) as response:
        result = response.read()
        prediction_result = json.loads(result)

    return jsonify(prediction_result)

if __name__ == "__main__":
    app.run(debug=True)