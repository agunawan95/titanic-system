from flask import Flask, jsonify, request
import pickle
import pandas as pd


app = Flask(__name__)

@app.route('/')
def home():
    return 'This is Predictor Service'

@app.route('/api/titanic', methods=['POST'])
def predict_data():
    data = request.json
    
    predict_data = {
        'Pclass': [data['pclass']],
        'Age': [data['age']],
        'female': [float( 1 if data['sex'] == 'female' else 0)],
        'male': [float( 1 if data['sex'] == 'male' else 0)],
    }
    df = pd.DataFrame(data=predict_data)
    
    loaded_model = pickle.load(open('model.bin', 'rb'))
    prediction_result = loaded_model.predict(df)[0]

    return jsonify({
        'Survived': int(prediction_result)
    })

if __name__ == "__main__":
    app.run(debug=True)