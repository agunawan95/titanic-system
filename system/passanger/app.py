from flask import Flask, jsonify
import pymysql
import configparser
import sys

config = configparser.ConfigParser()
config.read('app.conf')

app = Flask(__name__)

def get_connection():
    # Connect to MariaDB Platform
    try:
        conn = pymysql.connect(
            user= config['database']['username'],
            password=config['database']['password'],
            host=config['database']['host'],
            port=int(config['database']['port']),
            database=config['database']['database'],
            cursorclass=pymysql.cursors.DictCursor

        )
    except pymysql.Error as e:
        print(f"Error connecting to MariaDB Platform: {e}")
        sys.exit(1)

    return conn

@app.route('/')
def home():
    return 'This is Passenger Service'

@app.route('/api/passanger/<id>')
def get_passanger_by_id(id):
    conn = get_connection()
    cur = conn.cursor()

    sql = "SELECT * FROM passanger WHERE id = %s"
    cur.execute(sql, (str(id),))
    result = cur.fetchone()
    
    return jsonify(result)


if __name__ == "__main__":
    app.run(debug=True)